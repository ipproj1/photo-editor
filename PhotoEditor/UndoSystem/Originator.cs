﻿/**************************************************************************
 *                                                                        *
 *  File:        Originator.cs                                             *
 *  Copyright:   (c) 2021, Stanciu Ioan                                     *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/


using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UndoSystem
{
    /// <summary>
    /// Class responsible for the undo functionality.
    /// </summary>
    public class Originator
    {
        private Caretaker _caretaker = new Caretaker();

        /// <summary>
        /// Saves the input image
        /// </summary>
        /// <param name="bitmap">Input image</param>
        public void SaveStateToMemento(Bitmap bitmap)
        {
            _caretaker.AddMemento(new Memento(bitmap));
        }

        /// <summary>
        /// Gets the latest image from the caretaker
        /// </summary>
        /// <returns>Latest state of the image</returns>
        public Bitmap GetStateFromMemento()
        {
            Memento memento = _caretaker.GetMemento();
            return memento.State;
        }
    }
}
