﻿/**************************************************************************
 *                                                                        *
 *  File:        Caretaker.cs                                             *
 *  Copyright:   (c) 2021, Stanciu Ioan                                     *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UndoSystem
{
    /// <summary>
    /// Responsible for managing the states of the image
    /// </summary>
    class Caretaker
    {
        private Stack<Memento> _mementoStack = new Stack<Memento>();

        /// <summary>
        /// Adds a memento to the stack
        /// </summary>
        /// <param name="state">Input state (an image)</param>
        public void AddMemento(Memento state)
        {
            _mementoStack.Push(state);
        }

        /// <summary>
        /// Gets the latest memento
        /// </summary>
        /// <returns>Memento (an image)</returns>
        public Memento GetMemento()
        {
            if (_mementoStack.Count != 0)
            {
                return _mementoStack.Pop();
            }
            throw new Exception("No memento in caretaker stack.");
        }
    }
}
