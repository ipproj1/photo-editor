﻿/**************************************************************************
 *                                                                        *
 *  File:        Memento.cs                                             *
 *  Copyright:   (c) 2021, Stanciu Ioan                                     *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace UndoSystem
{
    /// <summary>
    /// Wrapper class for the memento design pattern
    /// </summary>
    class Memento
    {
        private Bitmap _state;

        public Memento(Bitmap state)
        {
            _state = (Bitmap)state.Clone();
        }

        public Bitmap State
        {
            get
            {
                return _state;
            }
        }
    }
}
