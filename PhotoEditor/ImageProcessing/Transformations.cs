﻿/**************************************************************************
 *                                                                        *
 *  File:        Transformations.cs                                       *
 *  Copyright:   (c) 2021, Stanciu Ioan                                   *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/


using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using Emgu.CV.Structure;

namespace ImageProcessing
{
    /// <summary>
    /// This class contains various tranformations that can be appied to images
    /// </summary>
    public class Transformations
    {
        /// <summary>
        /// Rotates the image by 90 degrees to the left
        /// </summary>
        /// <param name="image">Input image</param>
        /// <returns>Rotated image</returns>
        public static Bitmap RotateLeft90(in Bitmap image)
        {
            Mat inputImage = image.ToMat();
            Mat outputImage = new Mat();

            CvInvoke.Rotate(inputImage, outputImage, RotateFlags.Rotate90CounterClockwise);

            return outputImage.ToBitmap();
        }

        /// <summary>
        /// Rotates the image by 90 degrees to the right
        /// </summary>
        /// <param name="image">Input image</param>
        /// <returns>Rotated image</returns>
        public static Bitmap RotateRight90(in Bitmap image)
        {
            Mat inputImage = image.ToMat();
            Mat outputImage = new Mat();

            CvInvoke.Rotate(inputImage, outputImage, RotateFlags.Rotate90Clockwise);

            return outputImage.ToBitmap();
        }

        /// <summary>
        /// Rotates the image by 90 degrees to the right
        /// </summary>
        /// <param name="image">Input image</param>
        /// <returns>Rotated image</returns>
        public static Bitmap Rotate180(in Bitmap image)
        {
            Mat inputImage = image.ToMat();
            Mat outputImage = new Mat();

            CvInvoke.Rotate(inputImage, outputImage, RotateFlags.Rotate180);

            return outputImage.ToBitmap();
        }
    }
}
