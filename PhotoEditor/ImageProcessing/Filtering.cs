﻿/**************************************************************************
 *                                                                        *
 *  File:        Filtering.cs                                             *
 *  Copyright:   (c) 2021, Anitoaei Teodor                                *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using Emgu.CV.Structure;

namespace ImageProcessing
{
    /// <summary>
    /// This class is used to apply different filters to bitmaps.
    /// </summary>
    public class Filtering
    {
        /// <summary>
        /// Applies a gray filter to the given bitmap.
        /// Internally, it uses OpenCV cvtColor function.
        /// </summary>
        /// <param name="image">Input image</param>
        /// <returns>Grapy image</returns>
        public static Bitmap ApplyGray(in Bitmap image)
        {
            Mat inputImage = image.ToMat();
            Mat outputImage = new Mat();

            CvInvoke.CvtColor(inputImage, outputImage, ColorConversion.Bgr2Gray);
           // CvInvoke.Merge(new VectorOfMat(outputImage, outputImage, outputImage), outputImage);
            return outputImage.ToBitmap();
        }

        /// <summary>
        /// Applies a sepia filter by convolving the image with a specific kernel.
        /// </summary>
        /// <param name="image">Input image</param>
        /// <returns>Sepia image</returns>
        public static Bitmap ApplySepia( Bitmap image)
        {
            Image<Gray, float> kernelValues = new Image<Gray, float>(new float[,,]
                {
                    { {0.272f}, {0.534f}, {0.131f}},
                    { {0.349f}, {0.686f}, {0.168f}},
                    { { 0.393f}, { 0.769f}, { 0.189f} }
                });

            Mat kernel = kernelValues.Mat;

            Mat inputImage = image.ToMat();
            if(inputImage.NumberOfChannels !=3)
            {
                throw (new Exception("Image cannot be converted!"));
            }
            Mat outputImage = new Mat();

            CvInvoke.Transform(inputImage, outputImage, kernel);

            return outputImage.ToBitmap();
        }

        /// <summary>
        /// Applies a negative filter to the image by negating every pixel (bitwise not)
        /// </summary>
        /// <param name="image">Input image</param>
        /// <returns>Negative image</returns>
        public static Bitmap ApplyNegative(in Bitmap image)
        {
            Mat inputImage = image.ToMat();
            Mat outputImage = new Mat();

            CvInvoke.BitwiseNot(inputImage, outputImage);

            return outputImage.ToBitmap();
        }

        /// <summary>
        /// Applies a blur over the given image. Internally it calls the GaussianBlur
        /// function in OpenCV
        /// </summary>
        /// <param name="image">Input image</param>
        /// <param name="kernelSize">Gaussian kernel size</param>
        /// <param name="sigmaX">Sigma value</param>
        /// <returns>Blurred image</returns>
        public static Bitmap ApplyBlur(in Bitmap image, int kernelSize, double sigmaX)
        {
            Mat inputImage = image.ToMat();
            Mat outputImage = new Mat();
            if (inputImage.NumberOfChannels != 3)
            {
                throw (new Exception("Image cannot be converted!"));
            }
            CvInvoke.GaussianBlur(inputImage, outputImage, 
                new Size(kernelSize, kernelSize), sigmaX);

            return outputImage.ToBitmap();
        }

        /// <summary>
        /// Changes the image's pixels by enlarging them.
        /// </summary>
        /// <param name="image">Input image</param>
        /// <param name="pixelSize">New pixel size in pixels</param>
        /// <returns>Pixelated image</returns>
        public static Bitmap Pixelate(in Bitmap image, int pixelSize)
        {
            Mat inputImage = image.ToMat();
            Mat outputImage = new Mat();
            Mat temp = new Mat();

            if (pixelSize <= 0)
            {
                throw new ArgumentException("Pixel size cannot be less than 0.");
            }

            int smallWidth = inputImage.Width / pixelSize;
            int smallHeight = inputImage.Height / pixelSize;
            CvInvoke.Resize(inputImage, temp, new Size(smallWidth, smallHeight));
            CvInvoke.Resize(temp, outputImage, inputImage.Size, 0, 0, Inter.Nearest);

            return outputImage.ToBitmap();
        }
        
        /// <summary>
        /// Sharpens an image using unsharpen mask algorithm
        /// </summary>
        /// <param name="image">Input image</param>
        /// <param name="sigmaX">Blur intensity</param>
        /// <returns>Sharpened image</returns>
        public static Bitmap Sharpen(in Bitmap image, double sigmaX)
        {
            Mat inputImage = image.ToMat();
            Mat outputImage = new Mat();

            if (sigmaX <= 0)
            {
                throw new ArgumentException("SigmaX cannot be less than 0.");
            }

            CvInvoke.GaussianBlur(inputImage, outputImage,
                new Size(0, 0), sigmaX);

            CvInvoke.AddWeighted(inputImage, 1.5, outputImage, -0.5, 0, outputImage);

            return outputImage.ToBitmap();
        }
    }
}
