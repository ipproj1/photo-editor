﻿/**************************************************************************
 *                                                                        *
 *  File:        PaintMenu.cs                                           *
 *  Copyright:   (c) 2021, Mirt Alexandru                                 *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoEditor
{
    public partial class PaintMenu : Form
    {
        private int _brushSize;

        public PaintMenu()
        {
            InitializeComponent();
            trackBarBrushSize.Value = 4;
            _brushSize = trackBarBrushSize.Value;
            using (var g = panelBrushSizePreview.CreateGraphics())
            {
                g.Clear(BackColor);
                int size = panelBrushSizePreview.Width;
                g.FillEllipse(new SolidBrush(BrushColor),
                    size / 2 - _brushSize / 2,
                    size / 2 - _brushSize / 2,
                    _brushSize, _brushSize);
            }
        }

        private void buttonPickColor_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            colorDialog.AllowFullOpen = true;

            if (DialogResult.OK == colorDialog.ShowDialog())
            {
                panelColor.BackColor = colorDialog.Color;
            }
        }

        public Color BrushColor
        {
            get
            {
                return panelColor.BackColor;
            }
        }

        public int BrushSize
        {
            get
            {
                return _brushSize;
            }
        }

        private void trackBarBrushSize_Scroll(object sender, EventArgs e)
        {
            _brushSize = trackBarBrushSize.Value;

            using (var g = panelBrushSizePreview.CreateGraphics())
            {
                g.Clear(BackColor);
                int size = panelBrushSizePreview.Width;
                g.FillEllipse(new SolidBrush(BrushColor), 
                    size / 2 - _brushSize / 2,
                    size / 2 - _brushSize / 2,
                    _brushSize, _brushSize);
            }
        }
    }
}
