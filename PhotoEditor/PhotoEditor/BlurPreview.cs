﻿/**************************************************************************
 *                                                                        *
 *  File:        BlurPreview.cs                                           *
 *  Copyright:   (c) 2021, Mirt Alexandru                                 *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageProcessing;

namespace PhotoEditor
{
    public partial class BlurPreview : Form
    {
        private Bitmap _preview;
        private int _kerSize;
        private double _sigmaX;

        public BlurPreview(Bitmap preview)
        {
            InitializeComponent();
            _preview = preview;
            _kerSize = 1;
            _sigmaX = 0;
            pictureBoxPreview.Image = _preview;
            label3.Text = "0";
            label4.Text = "0";
            buttonCancel.DialogResult = DialogResult.Cancel;
            buttonApply.DialogResult = DialogResult.OK;
        }

        public int KerSize
        {
            get
            {
                return _kerSize;
            }
        }

        public double Sigma
        {
            get
            {
                return _sigmaX;
            }
        }

        private void trackBarKerSize_Scroll(object sender, EventArgs e)
        {
            _kerSize = trackBarKerSize.Value * 2 + 1;
            pictureBoxPreview.Image = Filtering.ApplyBlur(_preview, _kerSize, _sigmaX);
            label4.Text = _kerSize.ToString();
        }

        private void trackBarSigma_Scroll(object sender, EventArgs e)
        {
            _sigmaX = trackBarSigma.Value / 10.0;
            pictureBoxPreview.Image = Filtering.ApplyBlur(_preview, _kerSize, _sigmaX);
            label3.Text = _sigmaX.ToString();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
