﻿
namespace PhotoEditor
{
    partial class PaintMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.buttonPickColor = new System.Windows.Forms.Button();
            this.panelColor = new System.Windows.Forms.Panel();
            this.trackBarBrushSize = new System.Windows.Forms.TrackBar();
            this.labelBrush = new System.Windows.Forms.Label();
            this.panelBrushSizePreview = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBrushSize)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonPickColor
            // 
            this.buttonPickColor.Location = new System.Drawing.Point(318, 61);
            this.buttonPickColor.Name = "buttonPickColor";
            this.buttonPickColor.Size = new System.Drawing.Size(75, 23);
            this.buttonPickColor.TabIndex = 0;
            this.buttonPickColor.Text = "Pick Color";
            this.buttonPickColor.UseVisualStyleBackColor = true;
            this.buttonPickColor.Click += new System.EventHandler(this.buttonPickColor_Click);
            // 
            // panelColor
            // 
            this.panelColor.BackColor = System.Drawing.SystemColors.Highlight;
            this.panelColor.Location = new System.Drawing.Point(342, 31);
            this.panelColor.Name = "panelColor";
            this.panelColor.Size = new System.Drawing.Size(24, 24);
            this.panelColor.TabIndex = 1;
            // 
            // trackBarBrushSize
            // 
            this.trackBarBrushSize.LargeChange = 1;
            this.trackBarBrushSize.Location = new System.Drawing.Point(12, 39);
            this.trackBarBrushSize.Maximum = 20;
            this.trackBarBrushSize.Minimum = 2;
            this.trackBarBrushSize.Name = "trackBarBrushSize";
            this.trackBarBrushSize.Size = new System.Drawing.Size(222, 45);
            this.trackBarBrushSize.TabIndex = 2;
            this.trackBarBrushSize.Value = 2;
            this.trackBarBrushSize.Scroll += new System.EventHandler(this.trackBarBrushSize_Scroll);
            // 
            // labelBrush
            // 
            this.labelBrush.AutoSize = true;
            this.labelBrush.Location = new System.Drawing.Point(13, 13);
            this.labelBrush.Name = "labelBrush";
            this.labelBrush.Size = new System.Drawing.Size(55, 13);
            this.labelBrush.TabIndex = 3;
            this.labelBrush.Text = "Brush size";
            // 
            // panelBrushSizePreview
            // 
            this.panelBrushSizePreview.Location = new System.Drawing.Point(251, 22);
            this.panelBrushSizePreview.Name = "panelBrushSizePreview";
            this.panelBrushSizePreview.Size = new System.Drawing.Size(50, 50);
            this.panelBrushSizePreview.TabIndex = 4;
            // 
            // PaintMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 97);
            this.Controls.Add(this.panelBrushSizePreview);
            this.Controls.Add(this.labelBrush);
            this.Controls.Add(this.trackBarBrushSize);
            this.Controls.Add(this.panelColor);
            this.Controls.Add(this.buttonPickColor);
            this.Name = "PaintMenu";
            this.Text = "Paint Menu";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarBrushSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button buttonPickColor;
        private System.Windows.Forms.Panel panelColor;
        private System.Windows.Forms.TrackBar trackBarBrushSize;
        private System.Windows.Forms.Label labelBrush;
        private System.Windows.Forms.Panel panelBrushSizePreview;
    }
}