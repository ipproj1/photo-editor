﻿/**************************************************************************
 *                                                                        *
 *  File:        SharpenPreview.cs                                             *
 *  Copyright:   (c) 2021, Anitoaei Teodor                                *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageProcessing;

namespace PhotoEditor
{
    public partial class SharpenPreview : Form
    {
        private Bitmap _preview;
        private double _sigmaX;

        public SharpenPreview(Bitmap preview)
        {
            InitializeComponent();
            _preview = preview;
            pictureBoxPreview.Image = _preview;
            label3.Text = "0";
            buttonCancel.DialogResult = DialogResult.Cancel;
            buttonApply.DialogResult = DialogResult.OK;
        }

        private void trackBarSigma_Scroll(object sender, EventArgs e)
        {
            _sigmaX = trackBarSigma.Value / 10.0;
            try
            {
                pictureBoxPreview.Image = Filtering.Sharpen(_preview, _sigmaX);
            }
            catch (ArgumentException)
            {

            }
            label3.Text = _sigmaX.ToString();
        }

        public double Sigma
        {
            get
            {
                return _sigmaX;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
