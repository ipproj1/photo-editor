﻿/**************************************************************************
 *                                                                        *
 *  File:        Program.cs                                             *
 *  Copyright:   (c) 2021, Anitoaei Teodor                                *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using ImageProcessing;
using UndoSystem;
using Emgu.CV;

namespace PhotoEditor
{
    public partial class Photographix : Form
    {
        // === fields ===
        private Bitmap _image;
        private Originator _originator;
        private Rectangle rect;
        private Point _startLocation;
        private Point _endLocation;
        private bool _isMouseDown = false;
        private Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> _imgInput;
        private PaintMenu _paintMenu = new PaintMenu();
        private bool _isCropping = false;

        // === constructors ===
     
        public Photographix()
        {
            InitializeComponent();
            _originator = new Originator();

            pictureBoxImage.MouseWheel += new MouseEventHandler(pictureBoxImage_MouseWheel);
        }

        // === File menu methods ===
        private void stripMenuOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files | *.png;*.jpg";
            
            if (DialogResult.OK == openFileDialog.ShowDialog())
            {
                _imgInput = new Image<Emgu.CV.Structure.Bgr, byte>(openFileDialog.FileName);
                _image = new Bitmap(openFileDialog.FileName);
                int imgWidth = _image.Width;
                int imghieght = _image.Height;
                pictureBoxImage.Image = (Bitmap)_image.Clone();
                pictureBoxImage.Width = imgWidth;
                pictureBoxImage.Height = imghieght;
            }
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                _image = _originator.GetStateFromMemento();
                pictureBoxImage.Image = (Bitmap)_image.Clone();
                pictureBoxImage.Width = _image.Width;
                pictureBoxImage.Height = _image.Height;
            }
            catch (Exception exc)
            {
                System.Console.WriteLine(exc.Message);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.DefaultExt = "png";
            saveDialog.AddExtension = true;
            saveDialog.CheckPathExists = true;
            saveDialog.Filter = "Image file | *.png";

            if (DialogResult.OK == saveDialog.ShowDialog())
            {
                
                _image.Save(saveDialog.FileName);
            }
        }

        private void stripMenuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        // === Filter menu methods ===
        private void grayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _originator.SaveStateToMemento(_image);
            _image = Filtering.ApplyGray(_image);
            pictureBoxImage.Image = (Bitmap)_image.Clone();
        }

        private void sepiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _originator.SaveStateToMemento(_image);
            _image = Filtering.ApplySepia(_image);
            pictureBoxImage.Image = (Bitmap)_image.Clone();
        }

        private void negativeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _originator.SaveStateToMemento(_image);
            _image = Filtering.ApplyNegative(_image);
            pictureBoxImage.Image = (Bitmap)_image.Clone();
        }

        private void blurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BlurPreview blurDialog = new BlurPreview(_image);
            
            if (blurDialog.ShowDialog() == DialogResult.OK)
            {
                _originator.SaveStateToMemento(_image);
                _image = Filtering.ApplyBlur(_image, blurDialog.KerSize, blurDialog.Sigma);
                pictureBoxImage.Image = (Bitmap)_image.Clone();
            }
        }

        private void pixelateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PixelatePreview pixelateDialog = new PixelatePreview(_image);

            if (pixelateDialog.ShowDialog() == DialogResult.OK)
            {
                _originator.SaveStateToMemento(_image);
                _image = Filtering.Pixelate(_image, pixelateDialog.Pixels);
                pictureBoxImage.Image = (Bitmap)_image.Clone();
            }
        }

        private void sharpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SharpenPreview sharpenDialog = new SharpenPreview(_image);

            if (sharpenDialog.ShowDialog() == DialogResult.OK)
            {
                _originator.SaveStateToMemento(_image);
                _image = Filtering.Sharpen(_image, sharpenDialog.Sigma);
                pictureBoxImage.Image = (Bitmap)_image.Clone();
            }
        }

        // === Mouse events methods ===
        private void pictureBoxImage_MouseWheel(object sender, MouseEventArgs e)
        {
            debugLabel.Text = e.Delta.ToString();
        }
        private void pictureBoxImage_MouseEnter(object sender, EventArgs e)
        {
            //pictureBoxImage.Focus();
        }

        private void pictureBoxImage_MouseLeave(object sender, EventArgs e)
        {
            //Focus();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        // === Tranforms menu methods ===
        private void rotateLeft90DegreesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _originator.SaveStateToMemento(_image);
            _image = Transformations.RotateLeft90(_image);
            pictureBoxImage.Width = _image.Width;
            pictureBoxImage.Height = _image.Height;
            pictureBoxImage.Image = (Bitmap)_image.Clone();
        }

        private void rotateRight90DegreesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _originator.SaveStateToMemento(_image);
            _image = Transformations.RotateRight90(_image);
            pictureBoxImage.Width = _image.Width;
            pictureBoxImage.Height = _image.Height;
            pictureBoxImage.Image = (Bitmap)_image.Clone();
        }

        private void rotate180DegreesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _originator.SaveStateToMemento(_image);
            _image = Transformations.Rotate180(_image);
            pictureBoxImage.Image = (Bitmap)_image.Clone();
        }

        // === Painting methods ===
        private void paintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _paintMenu = new PaintMenu();
            _paintMenu.Show();
        }

        private void pictureBoxImage_MouseDown(object sender, MouseEventArgs e)
        {
            bool formOpened = Application.OpenForms.OfType<PaintMenu>().Any();
            if (e.Button == MouseButtons.Left && (formOpened || _isCropping))
            {
                _isMouseDown = true;
                _startLocation = e.Location;
                _originator.SaveStateToMemento(_image);
            }
        }

        private void pictureBoxImage_MouseUp(object sender, MouseEventArgs e)
        {
            if (_isMouseDown == true && e.Button == MouseButtons.Left)
            {
                _endLocation = e.Location;
                _isMouseDown = false;
                if (_isCropping && rect != null)
                {
                    try
                    {
                        //_originator.SaveStateToMemento(_image);
                        _imgInput = _image.ToImage<Emgu.CV.Structure.Bgr, byte>();
                        _imgInput.ROI = rect;
                        Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> temp = _imgInput.CopyBlank();
                        _imgInput.CopyTo(temp);
                        _imgInput.ROI = Rectangle.Empty;
                        _image = temp.ToBitmap();
                        pictureBoxImage.Invalidate();
                        _startLocation = Point.Empty;
                        _endLocation = Point.Empty;
                        pictureBoxImage.Image = (Bitmap)_image.Clone();
                    }
                    catch
                    {
                        //throw (new Exception("Nu se poate cropa in exteriorul pozei."));
                    }
                }
            }
        }

        private void pictureBoxImage_MouseMove(object sender, MouseEventArgs e)
        {
            bool formOpened = Application.OpenForms.OfType<PaintMenu>().Any();
            if (_isMouseDown && _paintMenu != null && formOpened)
            {
                Brush brush = new SolidBrush(_paintMenu.BrushColor);

                using (var graphics = Graphics.FromImage(_image))
                {
                    graphics.FillEllipse(brush, 
                        e.X - pictureBoxImage.Margin.Left,
                        e.Y - pictureBoxImage.Margin.Top,
                        _paintMenu.BrushSize, _paintMenu.BrushSize);
                }
                pictureBoxImage.Image = (Bitmap)_image.Clone();
                pictureBoxImage.Refresh();
                return;
            }
            if (_isMouseDown && _isCropping)
            {
                _endLocation = e.Location;
                pictureBoxImage.Invalidate();
            }
        }
    
        private Rectangle GetRectangle()
        {
            rect = new Rectangle
            {
                X = Math.Min(_startLocation.X, _endLocation.X),
                Y = Math.Min(_startLocation.Y, _endLocation.Y),
                Width = Math.Abs(_startLocation.X - _endLocation.X),
                Height = Math.Abs(_startLocation.Y - _endLocation.Y)
            };

            return rect;
        }

        private void pictureBoxImage_Paint(object sender, PaintEventArgs e)
        {
            if (rect != null && _isCropping)
            {
                //GetRectangle();
                e.Graphics.DrawRectangle(Pens.Red, GetRectangle());
            }
        }

        private void cropToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _isCropping = !_isCropping;
            cropToolStripMenuItem.Checked = !cropToolStripMenuItem.Checked;
        }

        // === Help and About ===
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, "..\\..\\..\\PhotographixManual.chm");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Copyright 2021. Team members:\n\n" +
                "Anitoaei Teodor \n" +
                "Mirt Alexandru \n" +
                "Miron Radu \n" +
                "Stanciu Ioan\n", 
                "About",
                MessageBoxButtons.OK);
        }
    }
}
