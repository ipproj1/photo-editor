/**************************************************************************
 *                                                                        *
 *  File:        Caretaker.cs                                             *
 *  Copyright:   (c) 2021, Miron Radu                                     *
 *  Description: Generates file information headers.                      *
 *                                                                        *
 *  This program is free software; you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation. This program is distributed in the      *
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without even   *
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR   *
 *  PURPOSE. See the GNU General Public License for more details.         *
 *                                                                        *
 **************************************************************************/


using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using ImageProcessing;
using UndoSystem;
using Emgu.CV;

namespace ImageProcessing
{
    [TestClass]
    public class UnitTest1
    {
        private Bitmap t1;
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))] 
        public void TestMethod1()
        {
            t1 = new Bitmap("..\\..\\..\\..\\..\\examples\\gr.png") ;
            Filtering.Sharpen(t1, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethod2()
        {
             t1 = new Bitmap("..\\..\\..\\..\\..\\examples\\gr.png");
            Filtering.Pixelate(t1, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestMethod3()
        {
            t1 = new Bitmap("..\\..\\..\\..\\..\\examples\\gr.png");
            Bitmap t2 = Filtering.ApplyBlur(t1, 0, 0.0);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestMethod4()
        {
            t1 = new Bitmap("..\\..\\..\\..\\..\\examples\\gr.png");
            Bitmap t2 = Filtering.ApplySepia(t1);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestMethod5()
        {
            t1 = new Bitmap("..\\..\\..\\..\\..\\examples\\gr.png");
            Bitmap t2 = Filtering.ApplySepia(t1);
        }


    
     
      


    }
}
